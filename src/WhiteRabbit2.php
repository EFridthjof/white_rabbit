<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){
            $Values = array(
                '1'   => 0,
                '2'   => 0,
                '5'   => 0,
                '10'  => 0,
                '20'  => 0,
                '50'  => 0,
                '100' => 0
            );

        if($amount <= 0){
            return $Values;
        }
        $coins = array(100, 50, 20, 10, 5, 2, 1);


        print "Amount = " . $amount . "\n";
        for($i = 0, count($coins); $i < count($coins); $i++){
            
            if($amount / $coins[$i] >= 1){
                
                print "\n Coin chosen: " . $coins[$i] . "   ";
                
                $amount = $amount - $coins[$i];
                
                print "\n New Amount: " . $amount;

                $Values[$coins[$i]] = $Values[$coins[$i]] + 1;

                if($amount >= $coins[$i])
                {
                    $i--; 
                }

            }
        }
        print "\n \n";
        return $Values;
        
    }
}