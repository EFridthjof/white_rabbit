<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return $this->findMedianLetter( $this->parseFile( $filePath ) );
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        return file_get_contents( $filePath);
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile)
    {
        //TODO implement this!
        $arr1 = array();
        $textLowered = strtolower($parsedFile);
        $count = count_chars($textLowered,1);


        $numberOfLetter = 0;
        foreach ($count as $key=>$occurrences){

            if (ctype_alpha($key)){

                if(preg_match("/^[a-zA-Z]+$/", chr($key))) { //Removing non-reading letters

                    $letterKey = chr($key);


                    array_push($arr1, array(
                        "letter" => (string)$letterKey,
                        "count" => (int)$occurrences
                    ));
                    $numberOfLetter= $occurrences + $numberOfLetter;
                }
            }
        }
        usort($arr1, function($a, $b){
            return $b["count"] - $a["count"];
        });

        $countletters = count($arr1);

        $medianInArray = intval( floor(( $countletters -1  ) /2));

        if($numberOfLetter % 2 == 0){
            print "Even number";
            return $arr1[$medianInArray] && $arr1[$medianInArray+1];
        }
        else{
            print "Uneven number";
            return $arr1[$medianInArray+1];
        }
    }
}
